/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdm.servicosmedicoscore.servicos;

import com.pdm.servicosmedicoscore.dao.DAO;
import com.pdm.servicosmedicoscore.dao.DAOJPA;
import com.pdm.servicosmedicosentidades.Avaliacao;
import com.pdm.servicosmedicosentidades.Medico;
import java.util.List;

/**
 *
 * @author Marcelo Augusto
 */
public class ClinicaService {
    
    DAO<Avaliacao> daoAvaliacao = new DAOJPA<>();
    DAO<Medico> daoMedico = new DAOJPA<>();
    
    public boolean salvarMedico (Medico medico){
        return daoMedico.salvar(medico);
    }
    
    public boolean atualizarMedico(Medico medico){
        return daoMedico.atualizar(medico);
    }
    
    public List<Medico> listarMedicos(){
        return daoMedico.consultaLista("medico.listar", null);
    }
    
    public Medico buscarMedicoPorId(long id){
        return daoMedico.buscar(id, Medico.class);
    }
    
    public boolean salvarAvaliacao (Avaliacao avaliacao){
        return daoAvaliacao.salvar(avaliacao);
    }
    
    public List<Avaliacao> listarAvaliacoes(){
        return daoAvaliacao.consultaLista("avaliacoes.listar", null);
    }
}
