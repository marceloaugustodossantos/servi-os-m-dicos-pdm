/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdm.servicosmedicoscore.servicos;

import com.pdm.servicosmedicoscore.dao.DAO;
import com.pdm.servicosmedicoscore.dao.DAOJPA;
import com.pdm.servicosmedicosentidades.Cliente;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Marcelo Augusto
 */
public class ClienteService {
    
    DAO<Cliente> daoCliente = new DAOJPA<>();
    
    public boolean salvarCliente (Cliente cliente){
        return daoCliente.salvar(cliente);
    }
    
    public List<Cliente> listarClientes(){
        return daoCliente.consultaLista("cliente.listar", null);
    }
    
    public Cliente buscarPorEmail(String email){
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("email", email);
        return daoCliente.consultaSimples("cliente.poremail", parametros);
    }

    public Cliente buscarPorId(long id) {
        return daoCliente.buscar(id, Cliente.class);
    }
    
}
