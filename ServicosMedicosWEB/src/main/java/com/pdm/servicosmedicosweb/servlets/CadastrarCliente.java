/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdm.servicosmedicosweb.servlets;

import com.pdm.servicosmedicoscore.servicos.ClienteService;
import com.pdm.servicosmedicoscore.servicos.ClinicaService;
import com.pdm.servicosmedicosentidades.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marcelo Augusto
 */
public class CadastrarCliente extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Cliente cliente = new Cliente();
        
        cliente.setNome(request.getParameter("nome"));
        cliente.setLogin(request.getParameter("login"));
        cliente.setSenha(request.getParameter("senha"));
        ClienteService clienteService = new ClienteService();
        ClinicaService clinicaService = new ClinicaService();
        
        clienteService.salvarCliente(cliente);
        
        request.getSession().setAttribute("cliente", cliente);
        request.getSession().setAttribute("medicos", clinicaService.listarMedicos());
        
        response.sendRedirect("visualizarprofissionais.jsp");
        
    }

}
