/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdm.servicosmedicosweb.servlets;

import com.pdm.servicosmedicoscore.servicos.ClienteService;
import com.pdm.servicosmedicoscore.servicos.ClinicaService;
import com.pdm.servicosmedicosentidades.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marcelo Augusto
 */
public class LoginCliente extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ClinicaService clinicaService = new ClinicaService();
        ClienteService clienteService = new ClienteService();
        Cliente cliente = clienteService.buscarPorEmail(request.getParameter("login"));
        String senha = request.getParameter("senha");
        if (cliente != null && cliente.getSenha().equals(senha)) {
            request.getSession().setAttribute("cliente", cliente);
            request.getSession().setAttribute("medicos", clinicaService.listarMedicos());
            response.sendRedirect("visualizarprofissionais.jsp");
        }

    }

}
