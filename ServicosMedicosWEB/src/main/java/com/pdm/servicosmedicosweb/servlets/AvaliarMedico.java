/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdm.servicosmedicosweb.servlets;

import com.pdm.servicosmedicoscore.servicos.ClienteService;
import com.pdm.servicosmedicoscore.servicos.ClinicaService;
import com.pdm.servicosmedicosentidades.Avaliacao;
import com.pdm.servicosmedicosentidades.Cliente;
import com.pdm.servicosmedicosentidades.Medico;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marcelo Augusto
 */
public class AvaliarMedico extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           
        ClinicaService clinicaService = new ClinicaService();
        ClienteService clienteService = new ClienteService();
        
        Medico medico = clinicaService.buscarMedicoPorId(Long.parseLong(request.getParameter("idMedico")));
        Cliente cliente = clienteService.buscarPorId(Long.parseLong(request.getParameter("idCliente")));
        
        Avaliacao avaliacao = new Avaliacao();
        avaliacao.setComentario(request.getParameter("comentario"));
        avaliacao.setEstrelas(Integer.parseInt(request.getParameter("estrelas")));
        avaliacao.setCliente(cliente);
        medico.getAvaliacoes().add(avaliacao);
        
        clinicaService.atualizarMedico(medico);
        
        request.setAttribute("medico", medico);
        
        response.sendRedirect("perfilMedico.jsp");
        
    }
}
