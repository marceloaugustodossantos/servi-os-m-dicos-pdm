/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pdm.servicosmedicosweb.servlets;

import com.pdm.servicosmedicoscore.servicos.ClienteService;
import com.pdm.servicosmedicoscore.servicos.ClinicaService;
import com.pdm.servicosmedicosentidades.Cliente;
import com.pdm.servicosmedicosentidades.Medico;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marcelo Augusto
 */
public class CadastrarMedico extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Medico medico = new Medico();
        
        medico.setNome(request.getParameter("nome"));
        medico.setEspecialidade(request.getParameter("especialidade"));
        medico.setEmail(request.getParameter("email"));
        medico.setClinica(request.getParameter("clinica"));
        medico.setTelefone(request.getParameter("telefone"));
        
        ClinicaService clinicaService = new ClinicaService();
        
        clinicaService.salvarMedico(medico);
        List<Medico> medicos = clinicaService.listarMedicos();
        medicos.add(medico);
        request.getSession().setAttribute("medicos", medicos);
        
        response.sendRedirect("visualizarprofissionais.jsp");
        
    }
    
}
